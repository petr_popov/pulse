<?php

namespace petr_popov\pulse;

include_once 'Socket.php';
include_once 'Data.php';
include_once 'Error.php';

class Pulse
{
    public static $defaultAddress = '10.12.1.68';
    public static $defaultPort = '10001';

    public static function initLog($site_id = '', $status = '', $codename = '', $site_view = '', $text = '', $url = '', $useragent = '', $id_login = '', $user_ip = '', $user_role = '') {
        $data = new Data();
        $dt = new \DateTime();
        $date = $dt->format('Y-m-d H:i:s');
        $data->setSiteId($site_id);
        $data->setStatus($status);
        $data->setCodename($codename);
        $data->setSiteView($site_view);
        $data->setText($text);
        $data->setUrl($url);
        $data->setUseragent($useragent);
        $data->setIdLogin($id_login);
        $data->setUserIp($user_ip);
        $data->setDate($date);
        $data->setUserRole($user_role);
        return $data;
    }

    public static function initError($site_id = '', $source = '', $class_name = '', $message = '', $stacktrace = '') {
        $error = new Error();
        $dt = new \DateTime();
        $date = $dt->format('Y-m-d H:i:s');
        $error->setSiteId($site_id);
        $error->setDate($date);
        $error->setSource($source);
        $error->setClassName($class_name);
        $error->setMessage($message);
        $error->setStackTrace($stacktrace);
        return $error;
    }

    public static function send($data, $address, $port)
    {
        if ($address === null) {
            $address = self::$defaultAddress;
        }
        if ($port === null) {
            $port = self::$defaultPort;
        }
        $socket = new Socket();
        $socket->send($address, $port, $data->toJson());
    }

    public static function log($site_id = '', $status = '', $codename = '', $site_view = '', $text = '', $url = '', $useragent = '', $id_login = '', $user_ip = '', $user_role = '', $address = null, $port = null) {
        $data = self::initLog($site_id, $status, $codename, $site_view, $text, $url, $useragent, $id_login, $user_ip, $user_role);
        self::send($data, $address, $port);
    }

    public static function error($site_id = '', $source = '',$class_name = '', $message = '', $stacktrace = '', $address = null, $port = null) {
        $error = self::initError($site_id, $source, $class_name, $message, $stacktrace);
        self::send($error, $address, $port);
    }
}