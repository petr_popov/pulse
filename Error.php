<?php

namespace petr_popov\pulse;


class Error
{
    public $site_id;
    public $date;
    public $source;
    public $class_name;
    public $message;
    public $stacktrace;

    public function __construct() {
        $this->site_id = '';
        $this->date = '';
        $this->source = '';
        $this->class_name = '';
        $this->message = '';
        $this->stacktrace = '';
    }

    public function setSiteId($site_id) {
        $this->site_id = $site_id;
    }

    public function setDate($date) {
        $this->date = $date;
    }

    public function setSource($source) {
        $this->source = $source;
    }

    public function setClassName($class_name) {
        $this->class_name = $class_name;
    }

    public function setMessage($message) {
        $this->message = $message;
    }

    public function setStackTrace($stacktrace) {
        $this->stacktrace = $stacktrace;
    }

    public function toJson() {
        return json_encode(array("type" => "error", "data" => get_object_vars($this)));
    }
}