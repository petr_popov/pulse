<?php

namespace petr_popov\pulse;

class Socket
{
    public $address;
    public $socket;

    public function __construct() {
        if (!($this->socket = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP))) {
//            TODO: error handling
        }
    }

    public function send($address, $port, $message) {
        try {
            if (!socket_sendto($this->socket, $message, strlen($message), 0, $address, $port)) {
                throw new \ErrorException('unable to write to socket');
            }
        } catch (\ErrorException $ex) {
//            TODO: error handling
        }

    }
}