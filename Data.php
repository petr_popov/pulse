<?php

namespace petr_popov\pulse;

class Data
{
    public $site_id;
    public $status;
    public $codename;
    public $site_view;
    public $text;
    public $url;
    public $useragent;
    public $id_login;
    public $user_ip;
    public $date;
    public $user_role;

    public function __construct() {
        $this->site_id = '';
        $this->status = '';
        $this->codename = '';
        $this->site_view = '';
        $this->text = '';
        $this->url = '';
        $this->useragent = '';
        $this->id_login = '';
        $this->user_ip = '';
        $this->date = '';
        $this->user_role = '';
    }

    public function setSiteId($site_id) {
        $this->site_id = $site_id;
    }

    public function setStatus($status) {
        $this->status = $status;
    }

    public function setCodename($codename) {
        $this->codename =$codename;
    }

    public function setSiteView($site_view) {
        $this->site_view = $site_view;
    }

    public function setText($text) {
        $this->text = $text;
    }

    public function setUrl($url) {
        $this->url = $url;
    }

    public function setUseragent($useragent) {
        $this->useragent = $useragent;
    }

    public function setIdLogin($id_login) {
        $this->id_login = $id_login;
    }

    public function setUserIp($user_ip) {
        $this->user_ip = $user_ip;
    }

    public function setDate($date) {
        $this->date = $date;
    }

    public function setUserRole($user_role) {
        $this->user_role = $user_role;
    }

    public function toJson() {
        return json_encode(array("type" => "log", "data" => get_object_vars($this)));
    }
}